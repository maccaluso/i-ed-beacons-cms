-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Mar 28, 2017 alle 12:15
-- Versione del server: 10.1.13-MariaDB
-- Versione PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ied_beacon`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `attivita`
--

CREATE TABLE `attivita` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(32) NOT NULL,
  `Cognome` varchar(32) NOT NULL,
  `Tipo_orario` varchar(32) NOT NULL,
  `Orario_apertura` time NOT NULL,
  `Orario_pausa` time NOT NULL,
  `Orario_riapertura` time NOT NULL,
  `Orario_chiusura` time NOT NULL,
  `Descrizione` text NOT NULL,
  `Preview_image` varchar(256) NOT NULL,
  `Aperture_straordinarie` text NOT NULL,
  `Via` varchar(32) NOT NULL,
  `Citta` varchar(32) NOT NULL,
  `Stato` varchar(32) NOT NULL,
  `CAP` int(11) NOT NULL,
  `Latitude` float NOT NULL,
  `Longitude` float NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `attivita`
--

INSERT INTO `attivita` (`ID`, `Nome`, `Cognome`, `Tipo_orario`, `Orario_apertura`, `Orario_pausa`, `Orario_riapertura`, `Orario_chiusura`, `Descrizione`, `Preview_image`, `Aperture_straordinarie`, `Via`, `Citta`, `Stato`, `CAP`, `Latitude`, `Longitude`, `Email`, `Password`) VALUES
(4, 'Michel', 'Collina', 'Orario Continuato', '08:00:00', '12:00:00', '14:00:00', '18:00:00', 'asda', 'http://res.cloudinary.com/iedbeacon/image/upload/v1490695054/sjpmljwhmqfk16cjw7y3.png', 'asda							', 'asd', 'asd', 'asd', 50142, 123, 1234, 'collina.michel@gmail.com', '0f2e53ed2ebdfc956cd1d3064cf1322b'),
(7, 'Prova', 'Prova', 'Orario Continuato', '04:00:00', '04:00:00', '04:00:00', '04:00:00', 'asdasdasdasd', 'http://res.cloudinary.com/iedbeacon/image/upload/v1490695054/sjpmljwhmqfk16cjw7y3.png', 'asdasdasdasdasdasd', 'asd', 'asd', 'asd', 50142, 123, 123, 'michelcollina@ied.edu', '0f2e53ed2ebdfc956cd1d3064cf1322b'),
(8, 'Gianluca', 'Macaluso', 'Orario Continuato', '04:00:00', '04:00:00', '04:00:00', '04:00:00', 'asdasdasdasdasd', 'http://res.cloudinary.com/iedbeacon/image/upload/v1490695328/znujiyqs6mlzffaquub6.jpg', 'asdasdasdasdasdasd', 'asda', 'asda', 'asda', 123, 123, 123, 'maccaluso@gmail.com', '7e172629e7b3817e2a00815103ae0d7e'),
(10, 'Giuseppe', 'Tano', 'Orario Continuato', '04:00:00', '04:00:00', '04:00:00', '04:00:00', 'asdasdas				', 'http://res.cloudinary.com/iedbeacon/image/upload/v1490695586/mdyoi52agkhoyi598q5f.jpg', 'asdasdasd				', 'asdas', 'asdas', 'asdas', 50142, 123, 123, 'giuseppetano@ied.edu', '7e172629e7b3817e2a00815103ae0d7e');

-- --------------------------------------------------------

--
-- Struttura della tabella `att_cat`
--

CREATE TABLE `att_cat` (
  `ID` int(11) NOT NULL,
  `ID_att` int(11) NOT NULL,
  `ID_cat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_negozio`
--

CREATE TABLE `categorie_negozio` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categorie_negozio`
--

INSERT INTO `categorie_negozio` (`ID`, `Nome`) VALUES
(1, 'Personal'),
(2, 'Shop'),
(3, 'Leisure'),
(4, 'Beauty & Fitness'),
(5, 'Health'),
(6, 'Hotel & Worldwide'),
(7, 'Retail & Service'),
(8, 'Food & Drink');

-- --------------------------------------------------------

--
-- Struttura della tabella `gallery`
--

CREATE TABLE `gallery` (
  `ID` int(11) NOT NULL,
  `ID_att` int(11) NOT NULL,
  `url_gallery` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `ied_login`
--

CREATE TABLE `ied_login` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(32) NOT NULL,
  `Cognome` varchar(32) NOT NULL,
  `Email` varchar(32) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Registrazione` datetime NOT NULL,
  `UltimaModifica` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ied_login`
--

INSERT INTO `ied_login` (`ID`, `Nome`, `Cognome`, `Email`, `Password`, `Registrazione`, `UltimaModifica`) VALUES
(1, 'Michel', 'Collina', 'collina.michel@gmail.com', '0f2e53ed2ebdfc956cd1d3064cf1322b', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Gianluca', 'Macaluso', 'maccaluso@gmail.com', '7e172629e7b3817e2a00815103ae0d7e', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Giuseppe', 'Tano', 'giuseppe.tano89@gmail.com', '7e172629e7b3817e2a00815103ae0d7e', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Gianluca', 'Governi', 'gianlucagoverni@ied.edu', '7e172629e7b3817e2a00815103ae0d7e', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Francesco', 'Scarniglia', 'francescoscarniglia@ied.edu', '7e172629e7b3817e2a00815103ae0d7e', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `Maglia`
--

CREATE TABLE `Maglia` (
  `ID` int(32) NOT NULL,
  `descrizione` text NOT NULL,
  `random` text NOT NULL,
  `prova` varchar(32) NOT NULL,
  `image` varchar(32) NOT NULL,
  `Taglia` varchar(32) NOT NULL,
  `prezzo` varchar(32) NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Maglia`
--

INSERT INTO `Maglia` (`ID`, `descrizione`, `random`, `prova`, `image`, `Taglia`, `prezzo`, `ID_att`) VALUES
(1, 'a caso', 'asdasdas', 'caro', 'asdas', '32', '50 euro', '4');

-- --------------------------------------------------------

--
-- Struttura della tabella `Pallone`
--

CREATE TABLE `Pallone` (
  `ID` int(32) NOT NULL,
  `descrizione` text NOT NULL,
  `Prezzo` varchar(32) NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Pallone`
--

INSERT INTO `Pallone` (`ID`, `descrizione`, `Prezzo`, `ID_att`) VALUES
(1, 'Dei bellissimi palloni da calcio', '40 euro', '4');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_attivita`
--

CREATE TABLE `prodotti_attivita` (
  `ID` int(11) NOT NULL,
  `ID_att` int(11) NOT NULL,
  `Nome_prod` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotti_attivita`
--

INSERT INTO `prodotti_attivita` (`ID`, `ID_att`, `Nome_prod`) VALUES
(3, 4, 'Prova'),
(5, 4, 'Maglia'),
(6, 4, 'Pallone'),
(15, 4, 'Prodotto_z'),
(16, 4, 'Prodotto_y'),
(17, 4, 'Prodotto_x');

-- --------------------------------------------------------

--
-- Struttura della tabella `Prodotto_x`
--

CREATE TABLE `Prodotto_x` (
  `ID` int(32) NOT NULL,
  `Beacon` varchar(64) NOT NULL,
  `descrizione` text NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Prodotto_x`
--

INSERT INTO `Prodotto_x` (`ID`, `Beacon`, `descrizione`, `ID_att`) VALUES
(1, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D:49160:40898', 'asdasdasdasdasdasdasdasdasda', '4');

-- --------------------------------------------------------

--
-- Struttura della tabella `Prodotto_y`
--

CREATE TABLE `Prodotto_y` (
  `ID` int(32) NOT NULL,
  `Beacon` varchar(64) NOT NULL,
  `descrizione` text NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Prodotto_y`
--

INSERT INTO `Prodotto_y` (`ID`, `Beacon`, `descrizione`, `ID_att`) VALUES
(1, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D:20705:42763', 'asdasdasdasdasdasdasdasdasda', '4');

-- --------------------------------------------------------

--
-- Struttura della tabella `Prodotto_z`
--

CREATE TABLE `Prodotto_z` (
  `ID` int(32) NOT NULL,
  `Beacon` varchar(64) NOT NULL,
  `descrizione` text NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Prodotto_z`
--

INSERT INTO `Prodotto_z` (`ID`, `Beacon`, `descrizione`, `ID_att`) VALUES
(1, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D:26954:35848', 'asdasdasdasdasdasdasdasdasda', '4');

-- --------------------------------------------------------

--
-- Struttura della tabella `Prova`
--

CREATE TABLE `Prova` (
  `ID` int(32) NOT NULL,
  `descrizione` text NOT NULL,
  `ID_att` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Prova`
--

INSERT INTO `Prova` (`ID`, `descrizione`, `ID_att`) VALUES
(1, 'asdasdasd', '4');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `attivita`
--
ALTER TABLE `attivita`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `att_cat`
--
ALTER TABLE `att_cat`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `categorie_negozio`
--
ALTER TABLE `categorie_negozio`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `ied_login`
--
ALTER TABLE `ied_login`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Maglia`
--
ALTER TABLE `Maglia`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Pallone`
--
ALTER TABLE `Pallone`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `prodotti_attivita`
--
ALTER TABLE `prodotti_attivita`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Prodotto_x`
--
ALTER TABLE `Prodotto_x`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Prodotto_y`
--
ALTER TABLE `Prodotto_y`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Prodotto_z`
--
ALTER TABLE `Prodotto_z`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `Prova`
--
ALTER TABLE `Prova`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `attivita`
--
ALTER TABLE `attivita`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `att_cat`
--
ALTER TABLE `att_cat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `categorie_negozio`
--
ALTER TABLE `categorie_negozio`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT per la tabella `gallery`
--
ALTER TABLE `gallery`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ied_login`
--
ALTER TABLE `ied_login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `Maglia`
--
ALTER TABLE `Maglia`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `Pallone`
--
ALTER TABLE `Pallone`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `prodotti_attivita`
--
ALTER TABLE `prodotti_attivita`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT per la tabella `Prodotto_x`
--
ALTER TABLE `Prodotto_x`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `Prodotto_y`
--
ALTER TABLE `Prodotto_y`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `Prodotto_z`
--
ALTER TABLE `Prodotto_z`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `Prova`
--
ALTER TABLE `Prova`
  MODIFY `ID` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
