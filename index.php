<?php
	session_id(1);
  	session_start();
?>
<html>
	<head>
		<script src="./js/jquery-3.1.1.min.js"></script>
		<script src="./js/main.js"></script>
		<script type="text/javascript">
			<?php 
				if(isset($_SESSION['ID_ADMIN']))
				{
					?>
						console.log("Benvenuto <?php echo $_SESSION['Nome_ADMIN']; ?>");
						$(document).ready(function(){
							$(".goToUpdate").on("click",function()
							{
								window.location = "edituser.php";
							});
							$(".refresh").on("click",function(){
								$.ajax({
								  type: "POST",
								  url: "functions.php?type=logout",
								  success: function(data){ 
								  	var dataParsed = JSON.parse(data);
								  	if(!dataParsed['success'])
								  	{
								  		console.log("Something went wrong"); 
								  	}
								  	else
								  	{
										window.location.reload();		}
								 	},
								  error: function()
								  {
								  	console.log("Something went wrong"); 
								  }
							});
						});
					});
					<?php
				}
				else
				{
					?>
						console.log("nessun profilo loggato");
					<?php
				}
			?>
		</script>
		<!-- Link css esterno principale -->
		<link rel = "stylesheet" type="text/css" href = "./css/html5reset-1.6.1.css" />
		<!-- Link css esterno principale -->
		<link rel = "stylesheet" type="text/css" href = "./css/style.css" />
	</head>
	<body>
		<div class="container">
			<?php 
				if(!isset($_SESSION['Nome_ADMIN']) && !isset($_SESSION['Nome_ADMIN']))
				{
			?>
			<form action="/" id="myForm">
				<input type="textbox" name="email" placeholder="email"></input>
				<input type="password" name="password" placeholder="password"></input>
				<input type="submit" name="submit" value="login" class="send" ></input>
				<div class="clear"></div>
			</form>
			<?php 
				}
				else
				{
					?>
						<h1><?php echo "Bentornato ".$_SESSION['Nome_ADMIN'];?></h1>
						<input type="button" name="logout" value="logout" class="refresh" ></input>
						<input type="button" name="update" value="update" class="goToUpdate"></input>

					<?php
				}
			?>
		</div>
	</body>
</html>