<?php 
	session_id(2);
	session_start();

	if(isset($_SESSION['ID']) && isset($_SESSION['Nome']))
	{
		?>
		<html>
		<head>
			<script src="./js/jquery-3.1.1.min.js"></script>
			<script src="./js/main.js"></script>
			<link rel = "stylesheet" type="text/css" href = "./css/html5reset-1.6.1.css" />
			<!-- Link css esterno principale -->
			<link rel = "stylesheet" type="text/css" href = "./css/style.css" />
		</head>
		<body>
			<form action="/" id="myProduct">
				<label for="nome">Nome:</label>
				<input type="textbox" id="nome" name="nome" placeholder="nome"></input>
				<label for="Beacon">ID Beacon:</label>
				<input type="textbox" id="Beacon" name="Beacon" placeholder="REGION:MAJOR:MINOR"></input>
				<label for="descrizione_t">Descrizione:</label><br>
				<textarea id="descrizione" name="descrizione_t" rows="4" cols="50">
				</textarea><br>
				
				<input type="button" class="addValue" value="Add new field"></input>


				<input type="button" name="insertProduct" value="insert" class="insertProduct" ></input>
				<input type="button" name="home" value="home" class="home" ></input>
				<div class="clear"></div>
			</form>
			<div class="containerModal">
				<div class="modal">
					<h2>Choose the type of field do you want to add</h2>
					<select class="selectValue">
						<option>Short text</option>
						<option>Long text</option>
						<option>Image/Gallery</option>
						<option>Number</option>
						<option>Price</option>
					</select>
					<h2>Insert the name of the field </h2>
					<input type="textbox" class="inputName"></input>
					<input type="button" class="submitField" value="add field"></input> 
					<input type="button" class="removeModal" value="cancel"></input>
				</div>
			</div>
		</body>
		</html>

		<!---
		
		ALL'INSERIMENTO DI LINK PER LA GALLERY E ALLA SELEZIONE DELLE CATEGORIE VISUALIZZARLE DI LATO ED INOLTRE

		INSERITE PARAMETRI HIDDEN PER I LINK DELLA GALLERY E PER LE CATEGORIE SCELTE 

		-->
		<?php
	}
	else
	{
		echo "You aren't logged in. If you want to register a new product you need to be registered";
	}
?>