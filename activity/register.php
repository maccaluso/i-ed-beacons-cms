<?php 
	session_id(2);
	session_start();
	require 'src/Cloudinary.php';
	require 'src/Uploader.php';
	require 'src/Api.php';
	\Cloudinary::config(array( 
	  "cloud_name" => "iedbeacon", 
	  "api_key" => "564459432692766", 
	  "api_secret" => "rkO08LpBJGp_Wld0nw2JMKUW7pY" 
	));


	if(!isset($_SESSION['ID']) && !isset($_SESSION['Nome']))
	{
		?>
		<html>
		<head>
			<script src="./js/jquery-3.1.1.min.js"></script>
			<script src="./js/main.js"></script>
			<link rel = "stylesheet" type="text/css" href = "./css/html5reset-1.6.1.css" />
			<!-- Link css esterno principale -->
			<link rel = "stylesheet" type="text/css" href = "./css/style.css" />

		</head>
		<body>
			<form action="/" id="myForm">
				<label for="nome">Nome:</label>
				<input type="textbox" id="nome" name="nome" placeholder="nome"></input>
				<label for="cognome">Cognome:</label>
				<input type="textbox" id="cognome" name="cognome" placeholder="cognome"></input>
				<label for="tipo_orario">Tipo Orario:</label><br>
				<select  id="tipo_orario" name="tipo_orario">
					<option>Orario Continuato</option>
					<option>Orario Spezzato</option>
				</select><br>
				<label for="orario_apertura">Orario Apertura:</label>				
				<input type="textbox" id="orario_apertura" name="orario_apertura" placeholder="HH:MM:SS"></input>
				<label for="orario_pausa">Orario Pausa:</label>				
				<input type="textbox" id="orario_pausa" name="orario_pausa" placeholder="HH:MM:SS"></input>
				<label for="orario_riapertura">Orario Riapertura:</label>				
				<input type="textbox" id="orario_riapertura" name="orario_riapertura" placeholder="HH:MM:SS"></input>
				<label for="orario_chiusura">Orario Chiusura:</label>				
				<input type="textbox" id="orario_chiusura" name="orario_chiusura" placeholder="HH:MM:SS"></input>

				<label for="descrizione">Descrizione:</label><br>
				<textarea id="descrizione" name="descrizione" rows="4" cols="50">
				</textarea><br>

				<label for="preview">Preview Image:</label>
				<input type="file" name="preview" id="preview" placeholder="select your file" accept="image/*">
				<input type="button" id="addLink" name="addLink" value="upload"></input><br>
				<input type="text" style="display:none;" name="preview" id="afterSelect"></input>

				<label for="aperture_straordinarie">Aperture Straordinarie:</label><br>
				<textarea id="aperture_straordinarie" name="aperture_straordinarie" rows="4" cols="50">
				</textarea><br>

				<label for="via">Via:</label>
				<input type="textbox" id="via" name="via" placeholder="via"></input>

				<label for="citta">Città:</label>
				<input type="textbox" id="citta" name="citta" placeholder="nome"></input>

				<label for="nazione">Nazione:</label>
				<input type="textbox" id="nazione" name="nazione" placeholder="nazione"></input>

				<label for="cap">CAP:</label>
				<input type="textbox" id="cap" name="cap" placeholder="cap"></input>

				<label for="latitudine">Latitudine:</label>
				<input type="textbox" id="latitudine" name="latitudine" placeholder="latitudine"></input>

				<label for="longitudine">Longitudine:</label>
				<input type="textbox" id="longitudine" name="longitudine" placeholder="longitudine"></input>

				<label for="email">Email:</label>
				<input type="textbox" id="email" name="email" placeholder="email"></input>

				<label for="gallery" >Gallery:</label><br>
				<input type="file" name="linkGallery" id="linkGallery" placeholder="select your file" accept="image/*">
				<input type="textbox" id="gallery" name="gallery" placeholder=""></input>

				<br>
				<label for="category">Seleziona le categorie:</label>
				<select id="category" name="category">
					<option>Cat 1</option>
					<option>Cat 2</option>
				</select>
				<br>
				<label for="password">Password:</label>
				<input type="password" id="password" name="password" placeholder="password"></input>
				<input type="submit" name="insert" value="insert" class="insert" ></input>
				<input type="button" name="home" value="home" class="home" ></input>
				<div class="clear"></div>
			</form>
		</body>
		</html>

		<!---
		
		ALL'INSERIMENTO DI LINK PER LA GALLERY E ALLA SELEZIONE DELLE CATEGORIE VISUALIZZARLE DI LATO ED INOLTRE

		INSERITE PARAMETRI HIDDEN PER I LINK DELLA GALLERY E PER LE CATEGORIE SCELTE 

		-->
		<?php
	}
	else
	{
		echo "you already login with you activity.";
	}
?>