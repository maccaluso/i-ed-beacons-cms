<?php
	session_id(2);
	session_start();

	require 'src/Cloudinary.php';
	require 'src/Uploader.php';
	require 'src/Api.php';
	\Cloudinary::config(array( 
	  "cloud_name" => "iedbeacon", 
	  "api_key" => "564459432692766", 
	  "api_secret" => "rkO08LpBJGp_Wld0nw2JMKUW7pY" 
	));



	if(isset($_SESSION['ID']))
	{
		$db = mysqli_connect('localhost','username','password','ied_beacon') or die('Error connecting to MySQL server.');	
	 	$query = "SELECT * FROM attivita WHERE ( ID = '".$_SESSION['ID']."' ) ";
		$rs = mysqli_query($db, $query) or die('Error querying database.');

		$row = $rs->fetch_assoc();

?>

<html>
	<head>
		<script src="./js/jquery-3.1.1.min.js"></script>
		<script src="./js/main.js"></script>
		<!-- Link css esterno principale -->
		<link rel = "stylesheet" type="text/css" href = "./css/html5reset-1.6.1.css" />
		<!-- Link css esterno principale -->
		<link rel = "stylesheet" type="text/css" href = "./css/style.css" />
	</head>
	<body>
			<form action="/" id="myForm">
				<label for="nome">Nome:</label>
				<input type="textbox" id="nome" name="nome" value="<?php echo $row['Nome']; ?>" placeholder="nome"></input>
				<label for="cognome">Cognome:</label>
				<input type="textbox" id="cognome" name="cognome" value="<?php echo $row['Cognome']; ?>" placeholder="cognome"></input>
				<label for="tipo_orario">Tipo Orario:</label><br>
				<select  id="tipo_orario" name="tipo_orario">
					<option>Orario Continuato</option>
					<option>Orario Spezzato</option>
				</select><br>
				<label for="orario_apertura">Orario Apertura:</label>				
				<input type="textbox" id="orario_apertura" name="orario_apertura" value="<?php echo $row['Orario_apertura']; ?>" placeholder="HH:MM:SS"></input>
				<label for="orario_pausa">Orario Pausa:</label>				
				<input type="textbox" id="orario_pausa" name="orario_pausa" value="<?php echo $row['Orario_pausa']; ?>" placeholder="HH:MM:SS"></input>
				<label for="orario_riapertura">Orario Riapertura:</label>				
				<input type="textbox" id="orario_riapertura" name="orario_riapertura" value="<?php echo $row['Orario_riapertura']; ?>" placeholder="HH:MM:SS"></input>
				<label for="orario_chiusura">Orario Chiusura:</label>				
				<input type="textbox" id="orario_chiusura" name="orario_chiusura" value="<?php echo $row['Orario_chiusura']; ?>" placeholder="HH:MM:SS"></input>

				<label for="descrizione">Descrizione:</label><br>
				<textarea id="descrizione" name="descrizione" rows="4" cols="50">
					<?php echo $row['Descrizione']; ?>
				</textarea><br>

				<label for="preview">Preview Image:</label>
				<input type="textbox" id="preview" name="preview" value="<?php echo $row['Preview_image']; ?>"placeholder="url dell'immagine di preview"></input>
				<?php echo cl_image_tag($row['Preview_image']); ?>
				<br>

				<label for="aperture_straordinarie">Aperture Straordinarie:</label><br>
				<textarea id="aperture_straordinarie" name="aperture_straordinarie"  rows="4" cols="50">
					<?php echo $row['Aperture_straordinarie']; ?>
				</textarea><br>

				<label for="via">Via:</label>
				<input type="textbox" id="via" name="via" value="<?php echo $row['Via']; ?>" placeholder="via"></input>

				<label for="citta">Città:</label>
				<input type="textbox" id="citta" name="citta" value="<?php echo $row['Citta']; ?>" placeholder="nome"></input>

				<label for="nazione">Nazione:</label>
				<input type="textbox" id="nazione" name="nazione" value="<?php echo $row['Stato']; ?>" placeholder="nazione"></input>

				<label for="cap">CAP:</label>
				<input type="textbox" id="cap" name="cap" value="<?php echo $row['CAP']; ?>" placeholder="cap"></input>

				<label for="latitudine">Latitudine:</label>
				<input type="textbox" id="latitudine" name="latitudine" value="<?php echo $row['Latitude']; ?>" placeholder="latitudine"></input>

				<label for="longitudine">Longitudine:</label>
				<input type="textbox" id="longitudine" name="longitudine" value="<?php echo $row['Longitude']; ?>" placeholder="longitudine"></input>

				<label for="email">Email:</label>
				<input type="textbox" id="email" name="email" value="<?php echo $row['Email']; ?>" placeholder="email"></input>

				<label for="gallery">Gallery:</label>
				<input type="textbox" id="gallery" name="gallery" value="<?php echo $row['Gallery']; ?>" placeholder="add link to gallery"></input>
				<?php
					$links = explode(",",$row['Gallery']);
					for($i = 0; $i < sizeof($links);$i++)
					{
						echo cl_image_tag($links[$i]); 
					}
				?>
				<br>
				<label for="category">Seleziona le categorie:</label>
				<select id="category" name="category">
					<option>Cat 1</option>
					<option>Cat 2</option>
				</select>
				<br>
				<label for="password">Password:</label>
				<input type="password" id="password" name="password" placeholder="password"></input>
				<input type="submit" name="update" value="update" class="update" ></input>
				<input type="button" name="home" value="home" class="home" ></input>
				<div class="clear"></div>
			</form>
	</body>
</html>
<?php
	}
	else
	{
		echo "You don't have permission to view this page.";
	}
?>
