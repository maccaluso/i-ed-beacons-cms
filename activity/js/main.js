$(document).ready(onReady)


function onReady()
{
	console.log("Ready");
	$(".send").on("click",send);
	$(".send").on("submit",function(e){
      e.preventDefault();
	});


	$(".home").on("click",function(){
		window.location = "index.php";
	});
	$(".update").on("click",update);
	$(".update").on("submit",function(e){
      e.preventDefault();
	});
	$(".register").on("click",function()
	{
		window.location = "register.php";
	});


	$(".insert").on("click",insert);
	$(".insert").on("submit",function(e){
      e.preventDefault();
	});

	$(".insertProduct").on("click",insertProduct);
	$(".insertProduct").on("submit",function(e){
      e.preventDefault();
	});


	$(".product").on("click",function(){
		window.location = "newproduct.php";
	});

	$(".addValue").on("click", function(){
		$(".containerModal").css("display","block");
	});
	$(".removeModal").on("click", function(){
		$(".containerModal").css("display","none");
	});
	$(".submitField").on("click", addCustomField);


	$(".viewAll").on("click",function(){
		window.location = "viewproducts.php";	
	});

	$(".single_prod").on("click",function(){
		window.location = "single.php?prod="+$(this).text();
	});
	$(".editsingle").on("click",function(){
		window.location = "editsingle.php?prod="+$(".productName").text();
	});

	$(".savesingle").on("click",updateSingle);
	$(".savesingle").on("submit",function(e){
      e.preventDefault();
	});


	$("#addLink").on("click",uploadImage);
	$("#addLink").on("click",function(e){
      e.preventDefault();
	});

	$("body").on("change","#linkGallery",uploadGallery);
	$("body").on("change","#linkGallery",function(e){
      e.preventDefault();
	});
	

	//$("input[type=file").change(function(){
	//	console.log($(this).val());
	//});
}
function uploadGallery(e)
{
	e.preventDefault();
	console.log($("#linkGallery").val());
	console.log("ok");
	var formData = new FormData();
	formData.append('file',$("#linkGallery")[0].files[0]);
	$.ajax({
		url : 'upload.php',
       	type : 'POST',
       	data : formData,
       	processData: false,  // tell jQuery not to process the data
       	contentType: false,  // tell jQuery not to set contentType
       	success : function(data) {
           dataParsed = JSON.parse(data);
           
           previousVal = $("#gallery").val();
           if(previousVal != "")
           	previousVal += ","+dataParsed['url'];
           else
           	previousVal = dataParsed['url'];


           $("#gallery").val(previousVal);
           alert(dataParsed['url']);
       	}
	});
}
function uploadImage(e)
{
	e.preventDefault();
	console.log($("#preview").val());
	console.log("ok");
	var formData = new FormData();
	formData.append('file',$("#preview")[0].files[0]);
	$.ajax({
		url : 'upload.php',
       	type : 'POST',
       	data : formData,
       	processData: false,  // tell jQuery not to process the data
       	contentType: false,  // tell jQuery not to set contentType
       	success : function(data) {
           //console.log(data);
           dataParsed = JSON.parse(data);
           $("#addLink").remove();
           $("#preview").remove();
           $("#afterSelect").css("display","block");
           $("#afterSelect").val(dataParsed['url']);
           //var url = data.replace('\/',"/");
		   //console.log()	
           alert(dataParsed['url']);
       	}
	});



}
function updateSingle(e)
{
    e.preventDefault();

	console.log("send");
	console.log("insertProduct");
	var data = $("#editSingle").serialize();
	console.log(data);
	$.ajax({
	  type: "POST",
	  url: "functions.php?type=updateSingleProduct",
	  data: data,
	  success: function(dataR){ 
	  	console.log(dataR);
	  	dataParsed = JSON.parse(dataR);
	 	if(dataParsed['success'])
	  	{
	  		window.location = "single.php?prod="+$(".productName").val();
	  	}
	  },
	  error: function()
	  {
	  	console.log("Something went wrong"); 
	  }
	});
}


function addCustomField()
{
	var value = $(".selectValue").val();
		var el;
		var name= $(".inputName").val();
		var label = document.createElement("label");
		$(label).text("insert the value for " + name);
		$(label).attr("for",name);
		switch(value)
		{
			case "Short text":
				el = document.createElement("input");
				$(el).attr("type","textbox");
				$(el).attr("name",name);
				$(el).addClass("customField ");
				//TEXTBOX
				$(label).insertBefore(".addValue");
				$(el).insertBefore(".addValue");
				console.log("1");
				break;
			case "Long text":
				el = document.createElement("textarea");
				$(el).attr("name",name+"_t");
				$(el).attr("rows","4");
				$(el).attr("cols","50");
				$(el).addClass("customField");
				//TEXTAREA
				
				$(label).insertBefore(".addValue");
				$(el).insertBefore(".addValue");
				
				console.log("2");
				break;
			case "Image/Gallery":
				el = document.createElement("input");
				$(el).attr("type","file");
				$(el).attr("name",name);
				$(el).attr("id","linkGallery");
				$(el).attr("accept","image/*")
				$(el).addClass("customField");

				el2 = document.createElement("input");
				$(el2).attr("type","textbox");
				$(el2).attr("name","image_t");
				$(el2).attr("id","gallery");
				$(el2).addClass("customField");

				$(label).insertBefore(".addValue");
				$(el).insertBefore(".addValue");
				$(el2).insertBefore(".addValue");

				//TEXTBOX
				console.log("3");
				break;
			case "Number":
				el = document.createElement("input");
				$(el).attr("type","textbox");
				$(el).attr("name",name);
				$(el).addClass("customField");
				

				$(label).insertBefore(".addValue");
				$(el).insertBefore(".addValue");
				

				//TEXTBOX
				console.log("4");
				break;
			case "Price":
				el = document.createElement("input");
				$(el).attr("type","textbox");
				$(el).attr("name",name);
				$(el).addClass("customField");
				

				$(label).insertBefore(".addValue");
				$(el).insertBefore(".addValue");
				
				//TEXTBOX
				console.log("5");
				break;
		}


		$(".containerModal").css("display","none");


}




function insertProduct(e)
{
	e.preventDefault();
	console.log("send");
	console.log("insertProduct");
	var data = $("#myProduct").serialize();
	console.log(data);
	$.ajax({
	  type: "POST",
	  url: "functions.php?type=insertProduct",
	  data: data,
	  success: function(dataR){ 
	  	console.log(dataR);
	 //  	var dataParsed = JSON.parse(dataR);
	 //  	if(!dataParsed['success'])
	 //  	{
	 //  		console.log("Something went wrong"); 
	 //  	}
	 //  	else
	 //  	{
	 //  		console.log("Dati modificati correttamente");
		// 	window.location = "index.php";		
		// }

	  },
	  error: function()
	  {
	  	console.log("Something went wrong"); 
	  }
	});
}


function insert(e)
{
	e.preventDefault();
	console.log("send");
	var data = $("#myForm").serialize();
	console.log(data);
	$.ajax({
	  type: "POST",
	  url: "functions.php?type=insert",
	  data: data,
	  success: function(dataR){ 
		console.log(dataR);

	 //  	var dataParsed = JSON.parse(dataR);
	 //  	if(!dataParsed['success'])
	 //  	{
	 //  		console.log("Something went wrong"); 
	 //  	}
	 //  	else
	 //  	{
	 //  		console.log("Dati modificati correttamente");
		// 	window.location = "index.php";		
		// }

	  },
	  error: function()
	  {
	  	console.log("Something went wrong"); 
	  }
	});
}


function update(e)
{
	e.preventDefault();
	console.log("send");
	var data = $("#myForm").serialize();
	$.ajax({
	  type: "POST",
	  url: "functions.php?type=update",
	  data: data,
	  success: function(dataR){ 
	  	console.log(dataR);
	  	var dataParsed = JSON.parse(dataR);
	  	if(!dataParsed['success'])
	  	{
	  		console.log("Something went wrong"); 
	  	}
	  	else
	  	{
	  		console.log("Dati modificati correttamente");
			window.location = "index.php";		
		}

	  },
	  error: function()
	  {
	  	console.log("Something went wrong"); 
	  }
	});
}
function send(e)
{
	e.preventDefault();
	console.log("send");
	var data = $("#myForm").serialize();
	$.ajax({
	  type: "POST",
	  url: "functions.php?type=login",
	  data: data,
	  success: function(dataR){ 
	  	var dataParsed = JSON.parse(dataR);
	  	if(!dataParsed['success'])
	  	{
	  		console.log("Something went wrong"); 
	  	}
	  	else
	  	{
	  		console.log("Benvenuto " + data);
			window.location.reload();		}
			console.log(dataParsed['success']);
	  },
	  error: function()
	  {
	  	console.log("Something went wrong"); 
	  }
	});
}
function keyUpHandler(event)
{

	switch(event.which)
	{
		
	}
}
