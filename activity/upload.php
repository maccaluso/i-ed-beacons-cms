<?php
	session_id(2);
	session_start();
	require 'src/Cloudinary.php';
	require 'src/Uploader.php';
	require 'src/Api.php';
	\Cloudinary::config(array( 
	  "cloud_name" => "iedbeacon", 
	  "api_key" => "564459432692766", 
	  "api_secret" => "rkO08LpBJGp_Wld0nw2JMKUW7pY" 
	));


	$fileName = $_FILES['file']['name'];
	$fileType = $_FILES['file']['type'];
	$fileError = $_FILES['file']['error'];
	$fileContent = file_get_contents($_FILES['file']['tmp_name']);

	if($fileError == UPLOAD_ERR_OK){
	   //Processes your file here
	}else{
	   switch($fileError){
	     case UPLOAD_ERR_INI_SIZE:   
	          $message = 'Error al intentar subir un archivo que excede el tamaño permitido.';
	          break;
	     case UPLOAD_ERR_FORM_SIZE:  
	          $message = 'Error al intentar subir un archivo que excede el tamaño permitido.';
	          break;
	     case UPLOAD_ERR_PARTIAL:    
	          $message = 'Error: no terminó la acción de subir el archivo.';
	          break;
	     case UPLOAD_ERR_NO_FILE:    
	          $message = 'Error: ningún archivo fue subido.';
	          break;
	     case UPLOAD_ERR_NO_TMP_DIR: 
	          $message = 'Error: servidor no configurado para carga de archivos.';
	          break;
	     case UPLOAD_ERR_CANT_WRITE: 
	          $message= 'Error: posible falla al grabar el archivo.';
	          break;
	     case  UPLOAD_ERR_EXTENSION: 
	          $message = 'Error: carga de archivo no completada.';
	          break;
	     default: $message = 'Error: carga de archivo no completada.';
	              break;
	    }
	      echo json_encode(array(
	               'error' => true,
	               'message' => $message
	            ));
	}
	$rs = \Cloudinary\Uploader::upload($_FILES["file"]["tmp_name"],
    	array(
	       "crop" => "limit", "width" => "400", "height" => "400",
	       "eager" => array(
	         array( "width" => 200, "height" => 200, 
	                "crop" => "thumb", "gravity" => "face",
	                "radius" => 20, "effect" => "sepia" ),
	         array( "width" => 100, "height" => 150, 
	                "crop" => "fit", "format" => "png" )
	       ),                                     
	       "tags" => array( "special", "for_homepage" )
	    ));


    echo json_encode($rs);
?>